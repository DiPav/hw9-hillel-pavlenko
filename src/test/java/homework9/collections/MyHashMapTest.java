package homework9.collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class MyHashMapTest {

    Map<Integer, String> hash = new MyHashMap<>();

    @BeforeEach
    void setup() {
        hash.put(1, "Apple");
        hash.put(2, "Berries");
        hash.put(3, "Carrot");
        hash.put(4, "Donut");
        hash.put(5, "IceCream");
        hash.put(6, "Feta");
    }

    @Test
    void shouldGiveSize() {
        assertEquals(hash.size(), 6);
    }

    @Test
    void shouldGiveByKey() {
        hash.remove(1);
        assertEquals(hash.size(), 5);
        assertEquals(hash.get(2), "Berries");
    }

    @Test
    void shouldVerifyContainsKeyMethod() {
        assertTrue(hash.containsKey(1));
        assertFalse(hash.containsKey(8));
    }

    @Test
    void shouldGiveByValue() {
        assertTrue(hash.containsValue("Berries"));
        assertFalse(hash.containsValue("Volvo"));
    }

    @Test
    void shouldPutAll() {
        Map<Integer, String> hashMap = new MyHashMap<>();
        Map<Integer, String> copy = new MyHashMap<>();

        hashMap.put(1, "Apple");
        hashMap.put(2, "Berries");
        hashMap.put(3, "Carrot");
        hashMap.put(4, "Donut");
        hashMap.put(5, "IceCream");
        hashMap.put(6, "Feta");

        copy.putAll(hashMap);

        assertEquals(copy.size(), 6);
        assertEquals(copy.get(2), "Berries");
    }

    @Test
    void shouldVerifyEmptiness() {
        assertFalse(hash.isEmpty());
    }

    @Test
    void shouldClear() {
        hash.clear();

        assertEquals(hash.size(), 0);
    }

}